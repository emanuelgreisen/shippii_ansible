---
private_ip: {{ ansible_eth1.ipv4.address }}

shorewall:
  rules:
    - "# Inbound rules"
    - "#----------------------------------------------------------"
    - "# Accept HTTP from the reverse proxy"
    - "HTTP(ACCEPT)    sa24in:$fw_servers                      $FW"
    - "# Accept SSH from admin whitelist"
    - "SSH(ACCEPT)     space:$admin_whitelist                  $FW"
    - "SSH(ACCEPT)     sa24in:$fw_servers                      $FW"
    - "# Accept PING from production and admin whitelist"
    - "Ping(ACCEPT)    space:$admin_whitelist                  $FW"
    - "Ping(ACCEPT)    sa24in:$fw_servers                      $FW"
    - "# Reject PING from net"
    - "Ping(REJECT)    space                                   $FW"

    - "# Outbound rules"
    - "#----------------------------------------------------------"
    - "# Accept localhost to net"
    - "ACCEPT          $FW                                    space"

    - "# Internal networks rules"
    - "#------------------------"
  interfaces:
    - "space   eth0            detect          tcpflags,logmartians,nosmurfs"
    - "sa24in  eth1            detect          tcpflags,logmartians,nosmurfs"
  zones:
    - "fw      firewall"
    - "space   ipv4"
    - "sa24in  ipv4"
  policy:
    - "# Traffic originating from sa24in"
    - "sa24in space   REJECT          info"
    - "sa24in $FW     REJECT          info"
    - "sa24in all     REJECT          info"
    - "# Traffic originating from the FW"
    - "$FW     space   REJECT          info"
    - "$FW     sa24in  ACCEPT"
    - "$FW     all     REJECT          info"
    - "# Traffic originating from SPACE"
    - "space   all     DROP            info"
    - "# The FOLLOWING POLICY MUST BE LAST"
    - "all     all     REJECT          info"
php:
  fpm:
    www:
      listen: 127.0.0.1:9000