---
private_ip: {{ ansible_eth1.ipv4.address }}

shorewall:
  rules:
    - "# Inbound rules"
    - "#----------------------------------------------------------"
    - "# Accept HTTPS from the Internet"
    - "HTTPS(ACCEPT)   space                                   $FW"
    - "# Accept HTTP from the Internet (will be redirected to HTTPS)"
    - "HTTP(ACCEPT)   space                                    $FW"
    - "# Accept SSH from admin whitelist"
    - "SSH(ACCEPT)     space:$admin_whitelist                  $FW"
    - "# Accept PING from production and admin whitelist"
    - "Ping(ACCEPT)    space:$admin_whitelist                  $FW"
    - "# Reject PING from net"
    - "Ping(REJECT)    space                                   $FW"

    - "# Outbound rules"
    - "#----------------------------------------------------------"
    - "# Accept localhost to net"
    - "ACCEPT          $FW                                     space"
    - "# Allow sa24in full access to the internet"
    - "ACCEPT          sa24in                                  space"

    - "# Internal networks rules"
    - "#------------------------"
  interfaces:
    - "space   eth0            detect          tcpflags,logmartians,nosmurfs"
    - "sa24in  eth1            detect          tcpflags,logmartians,nosmurfs"
  zones:
    - "fw      firewall"
    - "space   ipv4"
    - "sa24in  ipv4"
  masq:
    - "eth0                    10.129.18.27/16         146.185.183.147"
  policy:
    - "# Traffic originating from sa24in"
    - "sa24in space   REJECT          info"
    - "sa24in $FW     REJECT          info"
    - "sa24in all     REJECT          info"
    - "# Traffic originating from the FW"
    - "$FW     space   REJECT          info"
    - "$FW     sa24in  ACCEPT"
    - "$FW     all     REJECT          info"
    - "# Traffic originating from SPACE"
    - "space   all     DROP            info"
    - "# The FOLLOWING POLICY MUST BE LAST"
    - "all     all     REJECT          info"
