---
private_ip: {{ ansible_eth1.ipv4.address }}

mariadb_innodb_buffer_pool_size: "1024M"

shorewall:
  rules:
    - "# Inbound rules"
    - "#----------------------------------------------------------"
    - "# Accept HTTPS from the reverse proxy"
    - "ACCEPT         sa24in:$app_servers                      $FW   tcp   3306"
    - "# Accept Riak(http/protobuff) from app-servers"
    - "ACCEPT         sa24in:$app_servers                      $FW   tcp   8087,8089"
    - "# Accept SSH from admin whitelist"
    - "SSH(ACCEPT)     space:$admin_whitelist                  $FW"
    - "SSH(ACCEPT)     sa24in:$fw_servers                      $FW"
    - "# Accept PING from production and admin whitelist"
    - "Ping(ACCEPT)    space:$admin_whitelist                  $FW"
    - "Ping(ACCEPT)    sa24in:$fw_servers                      $FW"
    - "# Reject PING from net"
    - "Ping(REJECT)    space                                   $FW"

    - "# Outbound rules"
    - "#----------------------------------------------------------"
    - "# Accept localhost to net"
    - "ACCEPT          $FW                                    space"

    - "# Internal networks rules"
    - "#------------------------"
  interfaces:
    - "space   eth0            detect          tcpflags,logmartians,nosmurfs"
    - "sa24in  eth1            detect          tcpflags,logmartians,nosmurfs"
  zones:
    - "fw      firewall"
    - "space   ipv4"
    - "sa24in  ipv4"
  policy:
    - "# Traffic originating from sa24in"
    - "sa24in space   REJECT          info"
    - "sa24in $FW     REJECT          info"
    - "sa24in all     REJECT          info"
    - "# Traffic originating from the FW"
    - "$FW     space   REJECT          info"
    - "$FW     sa24in  ACCEPT"
    - "$FW     all     REJECT          info"
    - "# Traffic originating from SPACE"
    - "space   all     DROP            info"
    - "# The FOLLOWING POLICY MUST BE LAST"
    - "all     all     REJECT          info"

riak_node_name: "riak@10.129.18.227"
riak_pb_bind_ip: 10.129.18.227
riak_pb_port:    8087
riak_http_bind_ip: 10.129.18.227
riak_http_port:    8089