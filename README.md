# Ansible Playbooks
https://github.com/ansible/ansible

Ansible is an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks such
as continuous deployments or zero downtime rolling updates.

## Installing Ansible

We have tested with ansible version: 2.2.0.0, here is where you can find the newest version if your distro does not
have a new enough one already.

http://docs.ansible.com/intro_installation.html#latest-releases-via-apt-ubuntu

	$ sudo apt-get install software-properties-common
	$ sudo apt-add-repository ppa:ansible/ansible
	$ sudo apt-get update
	$ sudo apt-get install ansible

## Other pre-requisites

To build/package the Shippii platform you also need a number of build-tools.

    $ sudo apt-get install ant
    $ sudo apt-get install composer

The source code is checked out from BitBucket from the commandline over SSH. Hence, you must upload your SSH public key
to BitBucket.

## Running the plays (with Digital Ocean)

To run the plays you need access to the servers they involve. This is done over SSH, so you must have your SSH-key
installed on the servers prior to running the plays. Also, most plays require you to become root - hence your user must
be in the sudoers file, and you must specify your password on the commandline when Ansible starts up. This further puts
the requirement that your password must be the same on all servers involved in the plays you invoke.

## Running the plays (with AWS Staging)

Since AWS machines come with a standard "ubuntu" user which is in the sudoers file, the setup is to use this user and
not specify any become-pass on the commandline. However, they start up without python installed, hence you have to
manually do the following steps:

    $ ssh ubuntu@x.y.z.v "sudo apt-get install python -y"

## Adding SSH key to BitBucket
In order to establish a ssh connection with Bitbucket your public key needs to be added to your account:
    1. Open your Bitbucket profile and click on "Avatar" then select "Bitbucket settings";
    2. Account settings page is displayed, under "Security" section click on "SSH keys";
    3. The SSH keys page is shown, press the "Add key" button
    4. Copy your public key


            $ cat ~/.ssh/id_rsa.pub            
            
   5. Paste the public key in the "Key" field and add it to your Bitbucket account by pressing the "Add key" button.
    
## Running the plays (with AWS Production)

Since we need audit logging of who deploys what and when we will not use the built-in user "ubuntu" in production.
Therefor you need to initialize the hosts like above, but then also run the "init.yml" playment (which runs with the
built-in user, and adds the production users):

    $ cd de-aws-prod
	$ ansible-playbook init.yml
	$ ansible-playbook --ask-sudo-pass firewall.yml app.yml db.yml riak.yml shipall24.yml


## Play examples

Make sure the firewall, app, and db layers are configured and running in the DigitalOcean setup in the Netherlands:

    $ cd nl-do-sa24
	$ ansible-playbook --ask-sudo-pass firewall.yml app.yml db.yml

Setup the Shipall24 stack in the DigitalOcean setup in the Netherlands:

    $ cd nl-do-sa24
	$ ansible-playbook --ask-sudo-pass shipall24.yml

### Imported Role: MariaDB

https://galaxy.ansible.com/tschifftner/mariadb/

    $ ansible-galaxy install tschifftner.mariadb -p roles/

This has a bug where it keeps removing it's own admin user (under the task - remove anonymous users). Hence that part
has been commented out.

### Imported Role: Riak

https://galaxy.ansible.com/tschifftner/mariadb/

    $ ansible-galaxy install tschifftner.mariadb -p roles/

This has a bug where it keeps removing it's own admin user (under the task - remove anonymous users). Hence that part
has been commented out.

## Production setup

For now the production is setup with very small VMs. As the system comes under load it would make sense to start
upgrading to bigger servers. Eventually, it can also make sense to add more nodes to the Riak-ring (5 today) and maybe
the application nodes (2 today).

It may also make sense to look not only at adding more CPUs and Memory, but also bump up the kind of storage to some of
AWS faster types.

see: https://docs.basho.com/riak/kv/2.1.4/using/performance/amazon-web-services/


## Ansible documentation

Very clever place to look for how to deal with arrays/lists/dicts in Ansible:
http://jinja.pocoo.org/docs/dev/templates/#lower
